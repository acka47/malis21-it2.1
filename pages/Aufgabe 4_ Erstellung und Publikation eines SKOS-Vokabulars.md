title:: Aufgabe 4: Erstellung und Publikation eines SKOS-Vokabulars

- ## Formales
  * **Abgabefrist**: 22.02.2022
  * **Benotung**: Die gewählte Aufgabe zählt mit 35%iger Gewichtung zur Gesamtnote für das Modul IT2.
  * **Workload**: Maximal 25 Stunden
- ## Aufgabenstellung
  ◦ Erstellung eines in SKOS kodierten kontrollierten Vokabulars
  * Publikation des Vokabulars mit SkoHub Vocabs (Docker-Version)
- ## Bewertungskriterien
  * Das Vokabular sollte mindestens zehn SKOS-Konzepte umfassen.
  * Die Textstrings (`skos:prefLabel`, `dct:title`, `dct:descritpion` sowie ggf. `skos:altLabel`) sollen neben Deutsch in einer anderen (beliebigen) Sprache vorliegen.
  * Das Gesamtvokabular soll neben `dct:title` und `dct:description` mindestens zwei weitere informative Aussagen enthalten.
  * Neben den grundlegenden SKOS-Properties (`hasTopConcept`, `topConceptOf`, `inScheme`, `prefLabel`), sollten mindestens drei zusätzliche Properties sinnvoll genutzt werden.
- ## Lernziele
  Die Teilnehmer:innen können nach der Bearbeitung der Aufgabe:
  * ein git-Repositorium auf GitHub forken und einrichten,
  * ein kontrolliertes Vokabular in strukturierter, maschinenlesbarer Form mit SKOS abbilden,
  * eine menschenlesbare Sicht des Vokabulars mit SkoHub Vocabs publizieren.
- ## Orientierungshilfen
  * ((61e87ed6-59f9-4f82-ab23-64fdc0c60eb4))
  * ((61e87ed6-7902-4e40-b2e5-9d520328611b))