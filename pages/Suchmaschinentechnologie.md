- ## Was haben wir denn da?
  ![](../assets/skohub-lookup.png)
- ## Übung: ((61f1847d-6518-4549-af66-21f9ca544940))
- ## Die Suchfunktion in SkoHub Vocabs
  * Basiert auf der JavaScript-Bibliothek [FlexSearch](https://github.com/nextapps-de/flexsearch)
  * Das Gute: Der [Index](https://w3id.org/kim/hochschulfaechersystematik/scheme.de.index) lässt sich sogar herunterladen und anschauen.
  * Eine in besser lesbares JSON überführte Version gibt es unter [https://malis21.acka47.net/data/index-hochschulfaecher-de.json](https://malis21.acka47.net/data/index-hochschulfaechersystematik-de.json)
- ## Ein Suchmaschinenindex (Ausschnitt)
  ```json
              "i": [
                  "https://w3id.org/kim/hochschulfaechersystematik/n004",
                  "https://w3id.org/kim/hochschulfaechersystematik/n030",
                  "https://w3id.org/kim/hochschulfaechersystematik/n030010001",
                  "https://w3id.org/kim/hochschulfaechersystematik/n040",
                  "https://w3id.org/kim/hochschulfaechersystematik/n049",
                  "https://w3id.org/kim/hochschulfaechersystematik/n072",
                  "https://w3id.org/kim/hochschulfaechersystematik/n078",
                  "https://w3id.org/kim/hochschulfaechersystematik/n079",
                  "https://w3id.org/kim/hochschulfaechersystematik/n080",
                  "https://w3id.org/kim/hochschulfaechersystematik/n081",
                  "https://w3id.org/kim/hochschulfaechersystematik/n083",
                  "https://w3id.org/kim/hochschulfaechersystematik/n084",
                  "https://w3id.org/kim/hochschulfaechersystematik/n123",
                  "https://w3id.org/kim/hochschulfaechersystematik/n182",
                  "https://w3id.org/kim/hochschulfaechersystematik/n203",
                  "https://w3id.org/kim/hochschulfaechersystematik/n242",
                  "https://w3id.org/kim/hochschulfaechersystematik/n261",
                  "https://w3id.org/kim/hochschulfaechersystematik/n61",
                  "https://w3id.org/kim/hochschulfaechersystematik/n8"
              ],
              "in": [
                  "https://w3id.org/kim/hochschulfaechersystematik/n004",
                  "https://w3id.org/kim/hochschulfaechersystematik/n030",
                  "https://w3id.org/kim/hochschulfaechersystematik/n040",
                  "https://w3id.org/kim/hochschulfaechersystematik/n049",
                  "https://w3id.org/kim/hochschulfaechersystematik/n072",
                  "https://w3id.org/kim/hochschulfaechersystematik/n078",
                  "https://w3id.org/kim/hochschulfaechersystematik/n079",
                  "https://w3id.org/kim/hochschulfaechersystematik/n080",
                  "https://w3id.org/kim/hochschulfaechersystematik/n123",
                  "https://w3id.org/kim/hochschulfaechersystematik/n182",
                  "https://w3id.org/kim/hochschulfaechersystematik/n203",
                  "https://w3id.org/kim/hochschulfaechersystematik/n242",
                  "https://w3id.org/kim/hochschulfaechersystematik/n261",
                  "https://w3id.org/kim/hochschulfaechersystematik/n61",
                  "https://w3id.org/kim/hochschulfaechersystematik/n8"
              ],
              "int": [
                  "https://w3id.org/kim/hochschulfaechersystematik/n004",
                  "https://w3id.org/kim/hochschulfaechersystematik/n030",
                  "https://w3id.org/kim/hochschulfaechersystematik/n040",
                  "https://w3id.org/kim/hochschulfaechersystematik/n049",
                  "https://w3id.org/kim/hochschulfaechersystematik/n072",
                  "https://w3id.org/kim/hochschulfaechersystematik/n182"
              ],
  ```
- ## Weitere Beispiele
  * Indexierte Strings: "Archiv", "Museum", "Öffentliche Bibliothek", "Spezialbibliothek", "Universitätsbibliothek", "Fahrbibliothek"
  * Indexe mit unterschiedlichem [Tokenizer](https://github.com/nextapps-de/flexsearch#tokenizer): [strict](https://codeberg.org/acka47/flexsearch-test/src/branch/main/index/strict.json), [forward](https://codeberg.org/acka47/flexsearch-test/src/branch/main/index/forward.json), [full](https://codeberg.org/acka47/flexsearch-test/src/branch/main/index/full.json), [reverse](https://codeberg.org/acka47/flexsearch-test/src/branch/main/index/reverse.json)
  * Weitere Möglichkeiten: Stemming (Stammformreduktion), Filter (Stoppwörter)
- ## Grundbestandteile einer Suchmaschine
  * Datenquelle(n): strukturierte Daten, Volltexte, beides
  * Index
  * Abfragesprache(n), z.B. [Apache Lucene Query Syntax](https://lucene.apache.org/core/2_9_4/queryparsersyntax.html)
  * Suchoberfläche
- ## Gute Struktur & Indexierung ermöglichen auch komplexe Abfragen
  ![lobid-gnd-Suchergebnisliste](../assets/lobid-query.png)
  [Quelle](https://lobid.org/gnd/search?q=placeOfBirth.id%3A%22https%3A%2F%2Fd-nb.info%2Fgnd%2F4031483-2%22+AND+dateOfBirth%3A[1933+TO+1945]+AND+professionOrOccupation.label%3Ak%C3%BCnstler*)