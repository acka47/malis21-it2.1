- ## HTTP
	- MDN Web Docs, *An overview of HTTP*: https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview
	- Julia Evans, *HTTP: Learn your browser's language!* ($12): https://wizardzines.com/zines/http/
- ## HTML
  * MDN Web Docs, Verschiedene Lernressourcen zu HTML: https://developer.mozilla.org/en-US/docs/Web/HTML
  * Erläuterungen zu den im Firefox eingebauten Tools (STRG+Umschalten+i): https://developer.mozilla.org/en-US/docs/Tools
- ## Kommandozeile
	- Library Carpentry, *The UNIX Shell*: https://librarycarpentry.org/lc-shell/
- ## curl
	- curl für Windows Einführung: https://www.ionos.de/digitalguide/server/tools/einstieg-in-curl-in-windows
	- *Everything curl – an extensive guide for all things curl*: https://everything.curl.dev/ Hier insbesondere interessant: [Networking simplified](https://everything.curl.dev/protocols/network), [HTTP basics](https://everything.curl.dev/http/basics), [HTTP Redirects](https://everything.curl.dev/http/redirects)
- ## Web-APIs
	- Ritika Atal (2020): CRUD mapping to HTTP Verbs, https://medium.com/@ritika.atal.work/crud-mapping-to-http-verbs-354a3c0009f5
- ## JSON Schema
	- Understanding JSON Schema: http://json-schema.org/understanding-json-schema/
- ## RDF
  * EasyRDF Converter zum überführen von RDF-Daten in unterschiedliche RDF-Visualisierungen: https://www.easyrdf.org/converter
- ## LOUD
  * Pohl, Adrian / Steeg, Fabian / Christoph, Pascal (2018): lobid – Dateninfrastruktur für Bibliotheken. In: Informationspraxis 4(1). https://doi.org/10.11588/ip.2018.1.52445
  * Steeg, Fabian / Pohl, Adrian / Christoph, Pascal (2019): lobid-gnd – Eine Schnittstelle zur Gemeinsamen Normdatei für Mensch und Maschine. In: Informationspraxis 5(1). https://doi.org/10.11588/ip.2019.1.52673
  * [JSON-LD – Einführung und Anwendungen](https://slides.lobid.org/kim-ws-2018): Workshopfolien von 2018 inklusive einiger Übungen
- ## SPARQL & Wikidata
  * [Wikidata Query Service in Brief](https://commons.wikimedia.org/wiki/File:Wikidata_Query_Service_in_Brief.pdf)
  * Wikidata:SPARQL tutorial: https://www.wikidata.org/wiki/Wikidata:SPARQL_tutorial
  * Ivo Velitchkov (2020): Buckets and Balls. https://www.strategicstructures.com/?p=1889
- ## Wikidata
  * Allison-Cassin, Stacy & Scott, Dan (2018): Wikidata: a platform for your library’s linked open data. In: Code4Lib Journal 40. URL: https://journal.code4lib.org/articles/13424
  * Pohl, Adrian (2018): Was ist Wikidata und wie kann es die bibliothekarische Arbeit unterstützen? ABI Technik, 38 (2), S. 208. https://www.uebertext.org/2018/07/was-ist-wikidata-und-wie-kann-es-die.html
  * Pohl, Adrian (2021): How We Built a Spatial Subject Classification Based on Wikidata. In: Code4Lib Journal 51. URL: https://journal.code4lib.org/articles/15875
- ## SKOS
  id:: 61e87ed6-59f9-4f82-ab23-64fdc0c60eb4
  * [Einführung in SKOS am Beispiel von Open Educational Resources (OER)](https://dini-ag-kim.github.io/skos-einfuehrung/#/)
  * [Folien zu einem SKOS-Workshop von November 2021](https://pad.gwdg.de/p/einfuehrung-in-skos-mit-skohub/)
- ## SkoHub Vocabs
  id:: 61e87ed6-7902-4e40-b2e5-9d520328611b
  * [SkoHub-Slides eines SKOS-Workshops von November 2021](https://pad.gwdg.de/p/einfuehrung-in-skos-mit-skohub#/29)
  * [Materialien vom SWIB20 SkoHub Workshop](https://github.com/skohub-io/swib20-workshop/blob/main/resources/README.md):
    * [Install SkoHub Vocabs](https://github.com/skohub-io/swib20-workshop/blob/main/resources/README.md#install-skohub-vocabs)
    * [Using SkoHub Vocabs Docker version](https://github.com/skohub-io/swib20-workshop/blob/main/resources/README.md#3-using-skohub-vocabs-docker-version)
    * [Publishing a controlled Vocabulary with SkoHub Vocabs](https://github.com/skohub-io/swib20-workshop/blob/main/resources/README.md#4-publishing-a-small-controlled-vocabulary-with-skohub-vocabs)
  * [Metadatenharmonisierung in ETL-Prozessen mit SkoHub im Projekt WirLernenOnline](https://doi.org/10.25625/DN1X3A)
- ## Suchmaschinentechnologie
  * [lobid-gnd: Formulierung komplexer Suchanfragen](https://blog.lobid.org/2018/07/06/lobid-gnd-queries.html)